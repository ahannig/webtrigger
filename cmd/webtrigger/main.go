package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/ahannig/webtrigger/pkg/server"
)

// Flags
var (
	listen     string
	pubkey     string
	authHeader string
)

// Defaults
const (
	// ListenDefault is the address the server will listen on.
	listenDefault = "127.0.0.1:7200"
)

// Usage
const (
	listenUsage     = "server listen address"
	pubkeyUsage     = "public key for token verification"
	authHeaderUsage = "HTTP authorization header bearing the token"
)

func init() {
	// Initialize flags
	flag.StringVar(&listen, "listen", listenDefault, listenUsage)
	flag.StringVar(&listen, "l", listenDefault, listenUsage+" (shorthand)")

	flag.StringVar(&pubkey, "pubkey", "", pubkeyUsage)
	flag.StringVar(&pubkey, "p", "", pubkeyUsage+" (shorthand)")

	flag.StringVar(
		&authHeader,
		"header",
		server.AuthorizationHeaderDefault,
		authHeaderUsage)
	flag.StringVar(
		&authHeader,
		"H",
		server.AuthorizationHeaderDefault,
		authHeaderUsage+" (shorthand)")
}

func usage() {
	flag.Usage()
	fmt.Println("\t<trigger>	the command to run with a verified token")
}

func main() {
	flag.Parse()
	trigger := flag.Args()

	if pubkey == "" {
		log.Println("public key missing")
		return
	}

	if len(trigger) == 0 {
		log.Println("trigger command empty")
		flag.Usage()
		return
	}

	fmt.Println("awaiting trigger @", listen)
	s := server.HTTPServer{
		PublicKey:           pubkey,
		Trigger:             trigger,
		AuthorizationHeader: authHeader,
	}

	log.Fatal(s.Start(listen))
}
