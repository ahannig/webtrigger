{config, lib, ...}:
let
    webtrigger = with import <nixpkgs> {}; callPackage ../../pkgs/webtrigger {};

    cfg = config.services.webtrigger;
    
    # Webtrigger options: A webtrigger has a public key
    # and a listen address and a command
    triggerOptions = with lib; with types; submodule { options = {
        enable = mkEnableOption {
          description = "enable web trigger";
          default = true;
        };
        listen = mkOption {
          description = "the listen address";
          type = str;
        };
        cmd = mkOption {
          description = "the command to run";
          type = str;
        };
        pubkey = mkOption {
          description = "an public key for token verification";
          type = str;
        };
        header = mkOption {
          description = "the authorization header field";
          type = str;
          default = "Authorization";
        };
        user = mkOption {
          description = "run the service as this user";
          type = str;
          default = "nobody";
        };
    }; }; 

    options = with lib; {
        enable = mkEnableOption "enable webtriggers";
        webtriggers = mkOption {
          description = "a set of triggers";
          type = types.attrsOf triggerOptions;
        };
    };

    # Systemd services for each trigger
    mkService = name: trigger: with lib; 
        nameValuePair "webtrigger-${name}" {
              wantedBy = [ "multi-user.target" ];
              after = [ "network.target" ];
              description = "Webtrigger: ${name} @ ${trigger.listen}";
              serviceConfig = {
                Type = "simple";
                User = trigger.user;
                ExecStart = ''
                    ${webtrigger}/bin/webtrigger \
                         -p ${trigger.pubkey} \
                         -H ${trigger.header} \
                         -l ${trigger.listen} ${trigger.cmd}
                '';
              };
            };
in
{
    options.services.webtrigger = options;

    # Configuration
    config = lib.mkIf cfg.enable {
      # Add to system packages
      environment.systemPackages = [ webtrigger ];
  
      # Create systemd units
      systemd.services = with lib; mapAttrs' mkService cfg.webtriggers;
    };
}
