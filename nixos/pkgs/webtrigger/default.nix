{buildGoModule, nix-gitignore, ...}:
buildGoModule rec {
    pname = "webtrigger";
    version = "1.1.3";
    
    src = nix-gitignore.gitignoreSource [] ../../..;

    vendorHash = "sha256-xnYhzXB1m/H8g1Bbue3bVfOVDrmroyNxVDEzh5A29cg=";
    runVend = true;
}
