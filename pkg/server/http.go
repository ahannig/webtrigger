package server

import (
	"errors"
	"log"
	"net/http"
	"os"
	"os/exec"

	"gitlab.com/ahannig/pktoken/pkg/token"
)

// Errors
var (
	// ErrNoAuthorizationToken will be returned when an
	// authorization token could not be found in the request.
	ErrNoAuthorizationToken = errors.New("no token found in HTTP header")
)

// HTTPServer is a http listener waiting for an
// incoming request and running the trigger if the
// authorization was OK.
type HTTPServer struct {
	// PublicKey is the ed25519 key used for verifying
	// the authorization token.
	PublicKey string

	// AuthorizationHeader is the name of the header key
	// the token will be provided. e.g. X-Gitlab-Token
	// Default: Authorization
	AuthorizationHeader string

	// Trigger is the command to run
	Trigger []string
}

const (
	// AuthorizationHeaderDefault is the default http header
	// name the token will be in.
	AuthorizationHeaderDefault = "Authorization"
)

// Start will run a HTTPServer. It is intended to be mounted
// through some reverse proxy or API gateway.
func (s *HTTPServer) Start(listen string) error {
	return http.ListenAndServe(listen, s)
}

// ServeHTTP will handle incomming HTTP requests
func (s *HTTPServer) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	// Get token from HTTP request
	authHeader := AuthorizationHeaderDefault
	if s.AuthorizationHeader != "" {
		authHeader = s.AuthorizationHeader
	}
	auth := req.Header.Get(authHeader)
	if auth == "" {
		s.handleError(res, ErrNoAuthorizationToken)
		return
	}

	// Validate token
	if err := token.Verify(s.PublicKey, auth); err != nil {
		s.handleError(res, err)
		return
	}

	// Run trigger command
	cmd := exec.Command(s.Trigger[0], s.Trigger[1:]...)
	cmd.Env = os.Environ()

	log.Println("exec:", cmd)
	output, err := cmd.CombinedOutput()
	if err != nil {
		s.handleError(res, err)
	} else {
		res.WriteHeader(http.StatusOK)
	}
	res.Write(output)
}

// Encode errormessage and set status
func (s *HTTPServer) handleError(res http.ResponseWriter, err error) {
	log.Println("error:", err)

	// Write status
	if errors.Is(err, ErrNoAuthorizationToken) {
		res.WriteHeader(http.StatusUnauthorized)
	} else if errors.Is(err, token.ErrInvalidToken) {
		res.WriteHeader(http.StatusForbidden)
	} else {
		res.WriteHeader(http.StatusInternalServerError)
	}

	res.Write([]byte(err.Error()))
}
